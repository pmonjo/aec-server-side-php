<?php

namespace Adobe;

/**
 * Analytics class (unfinished)
 * Uses GET format
 */
class Analytics {
	/**
	 * ECID object
	 */
	private $ecid;
	
	/**
	 * Link to configuration
	 */
	private $config;

	/**
	 * Page name
	 */
	private $page_name;
	
	/**
	 * Channel
	 */
	private $channel;
	
	/**
	 * URL
	 */
	private $url;
	
	/**
	 * Referrer
	 */
	private $referrer;
	
	/**
	 * eVars
	 */
	private $evars;
	
	/**
	 * props
	 */
	private $props;
	
	/**
	 * Events
	 */
	private $events;
	
	/**
	 * User agent of the browser
	 */
	private $user_agent;
	
	/**
	 * IP address of the client
	 */
	private $ip;
	
	/**
	 * Constructor
	 */
	public function __construct($config,$ecid,$user_agent,$ip_client) {
		$this->config = $config;
		$this->ecid = $ecid;
		$this->page_name = null;
		$this->channel = null;
		$this->url = null;
		$this->evars = array();
		$this->props = array();
		$this->events = array();
		$this->user_agent = $user_agent;
		$this->ip = $ip_client;
		$this->tnta = null;
	}

	/**
	 * Set page name
	 */
	public function setPageName($page_name) {
		$this->page_name = $page_name;
	}
	
	/**
	 * Set channel
	 */
	public function setChannel($channel) {
		$this->channel = $channel;
	}
	
	/**
	 * Set URL
	 */
	public function setURL($url) {
		$this->url = $url;
	}
	
	/**
	 * Set referrer
	 */
	public function setReferrer($referrer) {
		$this->referrer = $referrer;
	}
	/**
	 * Set eVar
	 */
	public function setEvar($i,$value) {
		$v = "v$i";
		$this->evars[$v] = $value;
	}
	
	/**
	 * Set prop
	 */
	public function setProp($i,$value) {
		$c = "c$i";
		$this->props[$c] = $value;
	}
	
	/**
	 * Set events
	 */
	public function setEvent($i,$value=null) {
		$e = "event$i";
		$this->events[$e] = $value;
	}
	
	/**
	 * Add a query string parameter to the URL
	 */
	private function addQSP(&$url,$curl,$key,$value,$first=FALSE) {
		$url .= $first ? '?' : '&';
		$url .= curl_escape($curl,$key) . '=' . curl_escape($curl,$value);
	}
	
	/**
	 * Generate the URL to call Analytics
	 */
	private function getURL($curl) {
		// Server
		$url = 'http';
		if ($this->config->getMarketingCloudConfig(Config::MARKETINGCLOUD_SSL) === true) {
			$url .= 's';
		}
		$url .= '://';
		$url .= $this->config->getAnalyticsConfig(Config::ANALYTICS_SERVER);
		// Report suite
		$url .= '/b/ss/' . $this->config->getAnalyticsConfig(Config::ANALYTICS_RSIDS) . '/0';
		// Start
		$this->addQSP($url,$curl,'AQB',1,TRUE);
		$this->addQSP($url,$curl,'ndh',0);
		// ECID and charset
		$this->addQSP($url,$curl,'mid',$this->ecid->getECID());
		$this->addQSP($url,$curl,'aamb',$this->ecid->getBlob());
		$this->addQSP($url,$curl,'aamlh',$this->ecid->getDCSRegion());
		$this->addQSP($url,$curl,'sdid',$this->ecid->getSDID());
		$this->addQSP($url,$curl,'mcorgid',$this->config->getMarketingCloudConfig(Config::MARKETINGCLOUD_ORG));
		$this->addQSP($url,$curl,'ce',$this->config->getAnalyticsConfig(Config::ANALYTICS_CHARSET));
		// Page and URL
		$this->addQSP($url,$curl,'pageName',$this->page_name);
		$this->addQSP($url,$curl,'g',$this->url);
		if ($this->referrer) {
			$this->addQSP($url,$curl,'r',$this->referrer);
		}
		if ($this->channel) {
			$this->addQSP($url,$curl,'ch',$this->channel);
		}
		// Evars
		foreach ($this->evars as $evar => $value) {
			$this->addQSP($url,$curl,$evar,$value);
		}
		// Props
		foreach ($this->props as $prop => $value) {
			$this->addQSP($url,$curl,$prop,$value);
		}
		// Events
		$events = '';
		foreach ($this->events as $event => $value) {
			$events .= $event;
			if ($value) {
				$events .= '=' . $value;
			}
			$events .= ',';
		}
		$this->addQSP($url,$curl,'events',rtrim($events,','));
		// Finalise the URL
		$this->addQSP($url,$curl,'AQE',1);
		return $url;
	}

	/**
	 * Configure curl for the call to the ECID service
	 */
	private function curlSetopt($curl,$url) {
		$curlopts = array(
			CURLOPT_URL 			=> $url,
			CURLOPT_RETURNTRANSFER 	=> 1,
			CURLOPT_COOKIEFILE 		=> '',
			CURLOPT_USERAGENT 		=> $this->user_agent,
			CURLOPT_PROTOCOLS		=> CURLPROTO_HTTP|CURLPROTO_HTTPS
		);
		if (isset($this->ip) && $this->ip != "127.0.0.1") {
			$curlopts[CURLOPT_HTTPHEADER] = array('X-Forwarded-For: ' . $this->ip);
		}
		curl_setopt_array($curl, $curlopts);
	}

	/**
	 * Send the hit to the AA server
	 */
	public function sendHit() {
		// Initialise curl
		$curl = curl_init();
		// Create the URL
		$url = $this->getURL($curl);
		// Prepare HTTP request
		$this->curlSetopt($curl,$url);
		// Make the HTTP request
		curl_exec($curl);
		// Free resources
		curl_close($curl);		
		return $url;
	}
}

