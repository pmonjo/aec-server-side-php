<?php

namespace Adobe;

/**
 * Mbox class. Only to be used with Target
 */
class Mbox {
	/**
	 * Mbox name
	 */
	private $name;
	
	/**
	 * Page URL
	 */
	private $url;
	
	/**
	 * Page referrer
	 */
	private $referrer;
	
	/**
	 * Content received from a Target request
	 */
	private $content;

	/**
	 * Response tokens
	 */
	private $response_tokens;
		
	/**
	 * Constructor
	 */
	public function __construct($name) {
		$this->name = $name;
		$this->parameters = array();
		$this->url = null;
		$this->referrer = null;
		$this->content = null;
		$this->response_tokens = null;
	}
	
	/**
	 * Add a parameter
	 */
	public function addParameter($key,$value) {
		$this->parameters[$key] = $value;
	}
	
	/**
	 * Set the page URL
	 */
	public function setPageURL($url) {
		$this->url = $url;
	}
	
	/**
	 * Set the page referrer
	 */
	public function setReferrer($referrer) {
		$this->referrer = $referrer;
	}
	
	/**
	 * Get the content after an invocation to the Target API. A value of 'null' means default content.
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * Get the mbox name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Get the response tokens
	 */
	public function getResponseTokens() {
		return $this->response_tokens;
	}

	/**
	 * Set the value from the response. Currently, only supporting one content.
	 */
	public function setOptions($options) {
		$option = $options[0];
		$this->content = isset($option['content']) ? $option['content'] : null;
		$this->response_tokens = isset($option['responseTokens']) ? $option['responseTokens'] : null;
	}
	
}
