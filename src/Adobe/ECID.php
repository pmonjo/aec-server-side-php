<?php

namespace Adobe;

/**
 * ECID class
 */
class ECID {
	
	/**
	 * Supported version of the ECID service
	 */
	const VERSION = "5.2.0";
	
	/**
	 * MID
	 */
	private $mid;
	
	/**
	 * MID refresh period
	 */
	private $mid_refresh;
	
	/**
	 * UUID
	 */
	private $uuid;
	
	/**
	 * Demdex BLOB
	 */
	private $blob;
	
	/**
	 * DCS region
	 */
	private $dcs_region;

	/**
	 * TTL
	 */
	private $ottl;

	/**
	 * TID
	 */
	private $tid;

	/**
	 * Subdomain
	 */
	private $subdomain;
	
	/**
	 * Link to configuration
	 */
	private $config;
	
	/**
	 * SDID
	 */
	private $sdid;
	
	/**
	 * Response string from dpm.demdex.net
	 */
	private $demdex_response;
	
	/**
	 * User agent of the browser
	 */
	private $user_agent;
	
	/**
	 * IP address of the client
	 */
	private $ip;

	/**
	 * Constructor
	 */
	public function __construct ($config,$user_agent,$ip_client) {
		$this->config = $config;
		$this->mid = null;
		$this->sdid = null;
		$this->user_agent = $user_agent;
		$this->ip = $ip_client;
	}
	
	/**
	 * Generate the URL to call the ECID service
	 */
	private function getURL($curl) {
		$url = 'http';
		if ($this->config->getMarketingCloudConfig(Config::MARKETINGCLOUD_SSL) === true) {
			$url .= 's';
		}
		$url .= '://dpm.demdex.net/id';
		$url .= '?d_visid_ver=' . curl_escape($curl,ECID::VERSION);
		$url .= '&d_fieldgroup=MC&d_rtbd=json&d_ver=2&d_verify=1&d_nsid=0';
		$url .= '&d_orgid=' . curl_escape($curl,$this->config->getMarketingCloudConfig(Config::MARKETINGCLOUD_ORG));
		$url .= '&ts=' . curl_escape($curl,time());
		if ($this->mid) {
			$url .= '&d_mid=' . $this->mid;
		}
		return $url;
	}
	
	/**
	 * Configure curl for the call to the ECID service
	 */
	private function curlSetopt($curl,$url) {
		$curlopts = array(
			CURLOPT_URL 			=> $url,
			CURLOPT_RETURNTRANSFER 	=> 1,
			CURLOPT_AUTOREFERER		=> TRUE,
			CURLOPT_FOLLOWLOCATION	=> TRUE,
			CURLOPT_COOKIEFILE 		=> "",
			CURLOPT_USERAGENT 		=> $this->user_agent,
			CURLOPT_PROTOCOLS		=> CURLPROTO_HTTP|CURLPROTO_HTTPS,
			CURLOPT_HTTPHEADER		=> array(
				'X-Forwarded-For: ' . $this->ip,
			)
		);
		curl_setopt_array($curl, $curlopts);
	}
	
	/**
	 * Parse the cookies from the HTTP response.
	 */
	private function parseCookies($curl) {
		$cookies = curl_getinfo($curl, CURLINFO_COOKIELIST);
		foreach ($cookies as $c) {
			$m = preg_match('/\.demdex\.net.*\s(\d+)\sdemdex\s(\d+)/', $c, $matches);
			if ($m == 1) {
				$this->uuid = $matches[2];
				// $this->demdex_expiration = $matches[1];
				break;
			}
		}
	}
	
	/**
	 * Request a new ECID and associated parameters
	 */
	private function requestECID() {
		// Initialise curl
		$curl = curl_init();
		// Create the URL
		$url = $this->getURL($curl);
		// Prepare HTTP request
		$this->curlSetopt($curl,$url);
		// Make the HTTP request
		$this->demdex_response = curl_exec($curl);
		if ($this->demdex_response === FALSE) {
			throw new \Exception("Adobe\\ECID\\requestECID: " . curl_error($curl) . ": " . curl_errno($curl));
		}
		// Parse the result
		$this->fromJSON($this->demdex_response);
		// Parse cookies
		$this->parseCookies($curl);
		// Free resources
		curl_close($curl);		
	}
	
	/**
	 * Load the ECID, either from a previous session or by making a call to dpm.demdex.net.
	 */
	public function load() {
		$this->requestECID();
	}
	
	/**
	 * Load from a JSON object
	 */
	public function fromJSON($json) {
		$jd = json_decode($json,TRUE);
		if ($jd === NULL || isset($jd['errors'])) {
			// For now, just ignore JSON errors
			return;
		}
		$this->mid = isset($jd['d_mid']) ? $jd['d_mid'] : null;
		$this->blob = isset($jd['d_blob']) ? $jd['d_blob'] : null;
		$this->dcs_region = isset($jd['dcs_region']) ? $jd['dcs_region'] : null;
		$this->uuid = isset($jd['d_uuid']) ? $jd['d_uuid'] : null;
		$this->ottl = isset($jd['d_ottl']) ? $jd['d_ottl'] : null;
		$this->tid = isset($jd['tid']) ? $jd['tid'] : null;
		$this->subdomain = isset($jd['subdomain']) ? $jd['subdomain'] : null;
		// Other parameters to process: id_sync_ttl
	}
	
	/**
	 * Load from AMCV cookie
	 */
	public function loadFromAMCVCookie($cookie) {
		$cookie = urldecode($cookie);
		$ca = explode('|',$cookie);
		$i = 0;
		while ($i < count($ca)) {
			if ($ca[$i] == "MCMID") {
				$this->mid = $ca[$i+1];
				$i+=2;
			}
			else if (substr($ca[$i],0,7) == "MCAAMLH") {
				$this->dcs_region = $ca[$i+1];
				$i+=2;
			}
			else if (substr($ca[$i],0,6) == "MCAAMB") {
				$this->blob = $ca[$i+1];
				$i+=2;
			}
			else {
				$i++;
			}
		}
	}
	
	/**
	 * Get the ECID value (internally known as MID)
	 */
	public function getECID() {
		return $this->mid;
	}
	
	/**
	 * Get the UUID (the demdex cookie value)
	 */
	public function getUUID() {
		return $this->uuid;
	}
	
	/**
	 * Get the AAM blob
	 */
	public function getBlob() {
		return $this->blob;
	}
	
	/**
	 * Get the DCS region
	 */
	public function getDCSRegion() {
		return $this->dcs_region;
	}

	/**
	 * Get TTL
	 */
	public function getTTL() {
		return $this->d_ottl;
	}

	/**
	 * Get TID
	 */
	public function getTID() {
		return $this->tid;
	}

	/**
	 * Get subdomain
	 */
	public function getSubdomain() {
		return $this->subdomain;
	}
	
	/**
	 * Get the response from dpm.demdex.net. Only valid when the ECID is a new one.
	 */
	public function getDemdexResponse() {
		return $this->demdex_response;
	}

	/**
	 * Get the SDID
	 */
	public function getSDID() {
		if (!isset($this->sdid)) {
			$sha = sha1(uniqid() . md5(rand())); // This should be 40 chars long
			$this->sdid = substr($sha,0,16) . '-' . substr($sha,16,16);
			$this->sdid = strtoupper($this->sdid);
		}
		return $this->sdid;
	}
	
	/**
	 * Convert the object into a JSON object, to be stored elsewhere
	 */
	public function toJSON() {
		$e = array(
			'd_mid'			=> $this->mid,
			'd_blob'		=> $this->blob,
			'dcs_region'	=> $this->dcs_region,
			'd_uuid'		=> $this->uuid,
			'd_ottl'		=> $this->ottl,
			'tid'			=> $this->tid,
			'subdomain'		=> $this->subdomain
		);
		return json_encode($e);
	}
}
