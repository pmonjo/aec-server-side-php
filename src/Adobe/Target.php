<?php

namespace Adobe;

/**
 * Target class.
 * Using delivery API 1.0
 */
class Target {

	const CHANNEL_WEB = 'web';
	const CHANNEL_MOBILE = 'mobile';

	/**
	 * VERSION
	 */
	const VERSION = "2.6.0";

	/**
	 * ECID object
	 */
	private $ecid;
	
	/**
	 * Link to configuration
	 */
	private $config;
	
	/**
	 * Last request to Target
	 */
	private $request;
	
	/**
	 * Last response from Target
	 */
	private $response;

	/**
	 * Request ID
	 */
	private $request_id;

	/**
	 * Session ID
	 */
	private $session_id;
	
	/**
	 * User agent of the browser
	 */
	private $user_agent;
	
	/**
	 * IP address of the client
	 */
	private $ip;
	
	/**
	 * Host
	 */
	private $host;

	/**
	 * Channel
	 */
	private $channel;

	/**
	 * URL
	 */
	private $url;

	/**
	 * Referrer
	 */
	private $referrer;

	/**
	 * at_property
	 */
	private $at_property;
	
	/**
	 * Constructor
	 */
	public function __construct($config,$ecid,$session_id,$ip_client,$channel) {
		$this->config = $config;
		$this->ecid = $ecid;
		$this->session_id = $session_id;
		$this->ip = $ip_client;
		$this->channel = $channel;
	}

	/**
	 * Set User-Agent
	 */
	public function setUserAgent($user_agent) {
		$this->user_agent = $user_agent;
	}

	/**
	 * Set Host
	 */
	public function setHost($host) {
		$this->host = $host;
	}

	/**
	 * Set URL
	 */
	public function setURL($url) {
		$this->url = $url;
	}

	/**
	 * Set Referrer
	 */
	public function setReferrer($referrer) {
		$this->referrer = $referrer;
	}

	/**
	 * Set at_property
	 */
	public function setATProperty($at_property) {
		$this->at_property = $at_property;
	}

	/**
	 * Configure curl for the call to the ECID service
	 */
	private function curlConfigure($curl) {
		$curlopts = array(
			CURLOPT_URL 			=> $this->getRequestURL($curl),
			CURLOPT_RETURNTRANSFER 	=> 1,
			CURLOPT_COOKIEFILE 		=> '',
			CURLOPT_USERAGENT 		=> $this->user_agent,
			CURLOPT_POSTFIELDS		=> $this->getRequestJSON(),
			CURLOPT_PROTOCOLS		=> CURLPROTO_HTTP|CURLPROTO_HTTPS,
			CURLOPT_HTTPHEADER		=> array(
				'Cache-Control: no-cache',
				'Content-Type: application/json'
			)
		);
		if (isset($this->ip) && $this->ip != "127.0.0.1") {
			$curlopts[CURLOPT_HTTPHEADER][] = 'X-Forwarded-For: ' . $this->ip;
		}
		curl_setopt_array($curl, $curlopts);
	}
	
	/**
	 * Generate the URL to call Target
	 */
	private function getRequestURL($curl) {
		$client_code = $this->config->getTargetConfig(Config::TARGET_CLIENTCODE);
		$url = 'http';
		if ($this->config->getMarketingCloudConfig(Config::MARKETINGCLOUD_SSL) === true) {
			$url .= 's';
		}
		$url .= '://';
		$url .= $client_code;
		$url .= '.tt.omtrdc.net/rest/v1/delivery';
		$url .= '?client=' . curl_escape($curl,$client_code);
		$url .= '&sessionId=' . curl_escape($curl,$this->session_id);
		$url .= '&version=' . curl_escape($curl,Target::VERSION);
		return $url;
	}
	
	/**
	 * Generate the request for the Adobe Target Deliver API v1.0, in associative array format
	 * @ref https://developers.adobetarget.com/api/delivery-api/#section/Single-or-Batch-Delivery
	 */
	private function buildExecuteRequest($mboxes) {
		$this->request = array(
			'requestId' => bin2hex(random_bytes(20)),
			'id' => array(
				'marketingCloudVisitorId' => $this->ecid->getECID()
			),
			'context' => array(
				'channel' => $this->channel
			),
			'experienceCloud' => array(
				'audienceManager' => array(
					'locationHint' => $this->ecid->getDCSRegion(),
					'blob' => $this->ecid->getBlob()
				),
				'analytics' => array(
					'supplementalDataId' => $this->ecid->getSDID(),
					'logging' => "server_side" 
				)
			),
			'execute' => new \ArrayObject()
		);
		if ($this->user_agent) {
			$this->request['context']['userAgent'] = $this->user_agent;
		}
		if ($this->host) {
			$this->request['context']['browser'] = array(
				'host' => $this->host
			);
		}
		if ($this->url) {
			$this->request['context']['address'] = array(
				'url' => $this->url
			);
			if ($this->referrer) {
				$this->request['context']['address']['referringUrl'] = $this->referrer;
			}
		}
		if ($this->at_property) {
			$this->request['property'] = array(
				'token' => $this->at_property
			);
		}
		if ($mboxes && count($mboxes) > 0) {
			$this->request['execute']['mboxes'] = array();
			for ($i = 0; $i < count($mboxes) ; $i++) {
				$m = array(
					'index' => $i,
					'name' => $mboxes[$i]->getName()
				);
				array_push($this->request['execute']['mboxes'],$m);
			}
		}
		else {
			$this->request['execute']['pageLoad'] = new \ArrayObject();
		}
	}

	/**
	 * Parse the HTTP response.
	 */
	private function parseResponse($curl,$mboxes,$json_response) {
		$this->response = json_decode($json_response,TRUE);
		if ($this->response === NULL) {
			throw new \Exception("Adobe\\Target\\parseResponse: Could not decode JSON");
		}
		$this->request_id = $this->response['requestId'];
		// For each mbox in the response, find the corresponding mbox in the request and set the content
		if ($mboxes && count($mboxes) > 0) {
			foreach ($this->response['execute']['mboxes'] as $mbox_response) {
				$index = $mbox_response['index'];
				if (isset($mbox_response['options'])) {
					$mboxes[$index]->setOptions($mbox_response['options']);
				}
			}
		}
	}

	/**
	 * Executes request contents for the given mboxes. Prefetch is currently not supported.
	 * @param $mboxes An array of mboxes or null for a page load request (i.e. global mbox)
	 */
	public function execute($mboxes) {
		$curl = null;
		try {
			// Initialise curl
			$curl = curl_init();
			// Generate the body
			$this->buildExecuteRequest($mboxes);
			// Prepare HTTP request
			$this->curlConfigure($curl);
			// Make the HTTP request
			$json_response = curl_exec($curl);
			// Check for any errors
			if (curl_errno($curl)) {
				throw new \Exception("Adobe\\Target\\execute: " . curl_error($curl) . " (" . curl_errno($curl) . ")");
			}
			// Parse the result
			$this->parseResponse($curl,$mboxes,$json_response);
		}
		finally {
			// Free resources
			if ($curl) {
				curl_close($curl);
			}
		}
	}
	
	/**
	 * Get the last request to Target in associative array format
	 */
	public function getRequest() {
		return $this->request;
	}
	
	/**
	 * Get the last response to the Target API invokation, in associative array format
	 */
	public function getResponse() {
		return $this->response;
	}

	/**
	 * Get the last request to Target in JSON format
	 */
	public function getRequestJSON($pretty = false) {
		$flags = $pretty ? JSON_PRETTY_PRINT : 0;
		return json_encode($this->request,$flags);
	}
	
	/**
	 * Get the last response to the Target API invokation, in JSON format
	 */
	public function getResponseJSON($pretty = false) {
		$flags = $pretty ? JSON_PRETTY_PRINT : 0;
		return json_encode($this->response,$flags);
	}

}
