<?php

/* Helper function to autoload classes */
function classAutoLoader($class_name){
	$class_file =  dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'aec-server-side' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . '.php';
	if (is_file($class_file) && !class_exists($class_name)) 
		require_once $class_file;
}
spl_autoload_register('classAutoLoader');

try {
	$user_agent = "Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0";
	$ip_client = "90.200.211.238";
	$host = "adobe.pedromonjo.com";
	// $url = "https://adobe.pedromonjo.com/msg4/index.html";
	$url = "https://adobe.pedromonjo.com/serverside/examples.php?exp=news";
	$at_property = "d5ae8415-b3e3-f2ee-e670-9f1d28672a55";
	
	
	/* Config test */
	echo "*** CONFIG TESTS ***\n";
	$config = new Adobe\Config(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'ADBServerConfig.json');
	echo "Config version: " . $config->getVersion() . "\n";
	echo "Config RSIDs: " . $config->getAnalyticsConfig(Adobe\Config::ANALYTICS_RSIDS) . "\n";
	echo "\n";

	/* ECID test */
	echo "*** ECID TESTS ***\n";
	$f_ecid = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'ecid.json';
	$ecid = new Adobe\ECID($config,$user_agent,$ip_client);
	if (file_exists($f_ecid)) {
		echo "Using existing ECID\n";
		$ecid_json = file_get_contents($f_ecid);
		$ecid->fromJSON($ecid_json);
		$new_ecid = false;
	}
	else {
		echo "No ECID available; requesting a new one\n";
		$ecid->load();
		echo "dpm.demdex.net reponse: " . $ecid->getDemdexResponse() . "\n";
		$ecid_json = $ecid->toJSON();
		file_put_contents($f_ecid,$ecid_json);
		$new_ecid = true;
	}
	echo "ECID: " . $ecid->getECID() . "\n";
	echo "SDID: " . $ecid->getSDID() . "\n";
	echo "UUID: " . $ecid->getUUID() . "\n";
	echo "DCS region: " . $ecid->getDCSRegion() . "\n";
	echo "ECID to JSON: " . $ecid->toJSON() . "\n";
	echo "\n";

	/* Target tests */
	echo "*** TARGET TESTS ***\n";
	$f_session_id = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'session_id.txt';
	if ($new_ecid || !file_exists($f_session_id)) {
		$session_id = sha1(uniqid() . md5(rand()));
		file_put_contents($f_session_id,$session_id);
	}
	else {
		$session_id = file_get_contents($f_session_id);
	}
	echo "Target sessionId: $session_id\n";
	$target = new Adobe\Target($config,$ecid,$session_id,$ip_client,Adobe\Target::CHANNEL_WEB);
	$target->setUserAgent($user_agent);
	$target->setHost($host);
	$target->setURL($url);
	$target->setATProperty($at_property);
	$target->execute(null);
	echo "Target request: " . $target->getRequestJSON(true) . "\n";
	echo "Target response: " . $target->getResponseJSON(true) . "\n";

	/* Analytics tests */
/*	echo "*** Analytics TESTS ***\n";
	$analytics = new Adobe\Analytics($config,$ecid,$user_agent,$ip_client);
	$analytics->setPageName("Test page name");
	$analytics->setURL($url);
	$analytics->setEvar(1,"abc");
	$analytics->setProp(1,$url);
	$analytics->setEvent(1);
	$url = $analytics->sendHit();
	echo "Analytics hit: $url\n";
*/
}
catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
